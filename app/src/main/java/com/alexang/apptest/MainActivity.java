package com.alexang.apptest;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Celebrities Application.
 * Choose right celebrity.
 * Parsing html from "http://www.posh24.se/kandisar"
 */
public class MainActivity extends AppCompatActivity {

    private static final String WEB_PAGE = "http://www.posh24.se/kandisar";

    private ImageView mImageView;

    private Button mButtonOption1;
    private Button mButtonOption2;
    private Button mButtonOption3;
    private Button mButtonOption4;

    private List<String> celebritiesName;
    private List<String> celebritiesLinks;

    int chosenCeleb = 0;
    int locationOfCorrectAnswer = 0;
    String[] answers = new String[4];

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        downloadWebContent();
    }

    private void downloadWebContent() {
        String result = null;
        WebDownloaderTask webDownloaderTask = new WebDownloaderTask();
        try {
            result = webDownloaderTask.execute(WEB_PAGE).get();
            String[] splitResult = result.split("<div class=\"sidebarContainer\">");

            Pattern p = Pattern.compile("<img src=\"(.*?)\""); //everything between
            Matcher m = p.matcher(splitResult[0]);

            while (m.find()) {
                celebritiesLinks.add(m.group(1));
            }

            p = Pattern.compile("alt=\"(.*?)\""); //everything between
            m = p.matcher(splitResult[0]);

            while (m.find()) {
                celebritiesName.add(m.group(1));
            }
            updateQuestion();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void downloadBitmapImage() {
        Bitmap celebImage = null;
        ImageDownloaderTask imageDownloaderTask = new ImageDownloaderTask();
        try {
            celebImage = imageDownloaderTask.execute(celebritiesLinks.get(chosenCeleb)).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mImageView.setImageBitmap(celebImage);
    }

    /**
     * On Buttons pressed.
     *
     * @param view
     */
    public void choseCelebrity(View view) {
        if (view.getTag().toString().equals(Integer.toString(locationOfCorrectAnswer))) {
            Toast.makeText(getApplicationContext(), "Correct!", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "It was " + answers[locationOfCorrectAnswer], Toast.LENGTH_LONG).show();
        }
        updateQuestion();
    }

    private void updateQuestion() {
        Random random = new Random();
        chosenCeleb = random.nextInt(celebritiesLinks.size());

        downloadBitmapImage();

        locationOfCorrectAnswer = random.nextInt(4);
        int incorrectAnswerLocation;
        for (int i = 0; i < 4; i++) {
            if (i == locationOfCorrectAnswer) {
                answers[i] = celebritiesName.get(chosenCeleb);
            } else {
                incorrectAnswerLocation = random.nextInt(celebritiesLinks.size());
                while (incorrectAnswerLocation == chosenCeleb) {
                    incorrectAnswerLocation = random.nextInt(celebritiesLinks.size());
                }
                answers[i] = celebritiesName.get(incorrectAnswerLocation);
            }
        }
        mButtonOption1.setText(answers[0]);
        mButtonOption2.setText(answers[1]);
        mButtonOption3.setText(answers[2]);
        mButtonOption4.setText(answers[3]);
    }

    private void initialize() {
        mImageView = (ImageView) findViewById(R.id.iv_image);

        mButtonOption1 = (Button) findViewById(R.id.btn_option1);
        mButtonOption2 = (Button) findViewById(R.id.btn_option2);
        mButtonOption3 = (Button) findViewById(R.id.btn_option3);
        mButtonOption4 = (Button) findViewById(R.id.btn_option4);

        celebritiesName = new ArrayList<>();
        celebritiesLinks = new ArrayList<>();
    }
}
